-- creazione db e tabelle

USE axis;

CREATE DATABASE IF NOT EXISTS axis;

CREATE TABLE 'people_counter' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'serial' varchar(50) DEFAULT NULL,
  'name' varchar(50) DEFAULT NULL,
  'time_control' timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  'in_event' int(11) DEFAULT '0',
  'out_event' int(11) DEFAULT '0',
  PRIMARY KEY ('id')
);

commit;