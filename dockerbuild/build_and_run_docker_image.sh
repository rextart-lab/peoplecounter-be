#!/bin/bash

# Build and run Docker image
# Author Mattia Demuru - mattia.demuru@rextart.com
# Version: 1.0.1

typeset -A config
config=(
    [image_name]=""
    [container_name]=""
    [port_forward_from]=""
    [port_forward_to]=""
)

# read properties from build_and_run_docker_image.conf
while read line
do
    if echo $line | grep -F = &>/dev/null
    then
        varname=$(echo "$line" | cut -d '=' -f 1)
        config[$varname]=$(echo "$line" | cut -d '=' -f 2-)
    fi
done < build_and_run_docker_image.conf

# build and upload to Docker Hub the image (the old image with the same name will be overwritten)
echo "Building image..."
docker build -t ${config[image_name]} .

# stop container based on old Docker image (with same name)
echo "Stopping old container..."
docker stop ${config[container_name]}

# delete container based on old Docker image (with same name)
echo "Removing old image..."
docker rm ${config[container_name]}

# run container based on new Docker image
echo "Running new container..."
docker run --publish ${config[port_forward_from]}:${config[port_forward_to]} --detach --name ${config[container_name]} ${config[image_name]}
