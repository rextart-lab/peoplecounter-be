Lo script si occupa di buildare e runnare le immagine Docker basandosi sul Dockerfile.

Specificare le prorietà nell'apposito file *.conf*:
- **nome immagine**: nel formato ```name:tag```
- **nome container**
- port mapping:
    - **containter port**
    - **host port**

Lo script esegue le seguenti operazioni:
- Build ed upload su Docker Hub della nuova immagine.
- Stop del container con stesso nome basato sulla vecchia immagine Docker.
- Elminazione vecchia immagine con stesso nome.
- Run nuovo containter. Il vecchio container con lo stesso nome verrà sovrascritto.