package com.rextart.axis.tools.peoplecounter.domain.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class DateTimeIntervalHourDetailModel
{
	@JsonFormat(pattern=Constants.INPUT_DATE_FORMAT)
	private Date date = new Date(0L);
	
	private Integer min = 60;
	
	private TimeIntervalModel times = new TimeIntervalModel();
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	public Integer getMin()
	{
		return min;
	}
	
	public void setMin(Integer min)
	{
		this.min = min;
	}
	
	public boolean areValid()
	{
		return date != null && min != null && min > 0 && min <= 60 && times != null && times.areValidInterval();
	}
	
	public void fillIfEmpty()
	{
		if(date == null) date = new Date(0L);
		if(times != null) times.fillIfEmpty();
		if(min == null) min = 60;
	}

	public TimeIntervalModel getTimes()
	{
		return times;
	}

	public void setTimes(TimeIntervalModel times)
	{
		this.times = times;
	}

	@Override
	public String toString()
	{
		return "DateTimeIntervalHourDetailModel [ date=" + (date == null ? null : Utility.dateToString(date, Constants.INPUT_DATE_FORMAT)) + ", times=" + times + ", min=" + min + "]";
	}
}
