package com.rextart.axis.tools.peoplecounter.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rextart.axis.tools.peoplecounter.domain.dto.PeopleCounterDTO;
import com.rextart.axis.tools.peoplecounter.domain.entity.PeopleCounterEntity;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalHourDetailModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;
import com.rextart.axis.tools.peoplecounter.repository.IPeopleCounterDAO;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.DomainConverter;
import com.rextart.axis.tools.peoplecounter.util.Utility;

@Service
public class PeopleCounterService implements IPeopleCounterService {

	@Autowired
	private IPeopleCounterDAO peopleCounterDAO;
	
	@Autowired
	com.fasterxml.jackson.databind.ObjectMapper objectMapper;
	
	 @PostConstruct
	 public void init() 
	 {
		 objectMapper.setTimeZone(TimeZone.getDefault());
	 }
	
	@Override
	public boolean addPeopleCounter(PeopleCounterDTO counter) {
		if(counter == null) return false;
		
		peopleCounterDAO.addPeopleCounter(DomainConverter.convertToEntity(counter));
	    return true;
	}

	@Override
	public void updatePeopleCounter(PeopleCounterDTO counter) {}

	@Override
	public void deletePeopleCounter(int id) {}

	@Override
	public boolean peopleCounterExists(String serial, String name) 
	{
		return false;
	}

	@Override
	public PeopleCounterDTO getTodayPeopleCounter()
	{
		PeopleCounterEntity entity = peopleCounterDAO.getPeopleCounterByDate(new Date());
		
		return DomainConverter.convertToDTO(entity);
	}
	
	@Override
	public PeopleCounterDTO getPeopleCounterByDate(Date date)
	{
		if(date == null) return null; //TODO ritornare errore da gestire dal controller
		
		PeopleCounterEntity entity = peopleCounterDAO.getPeopleCounterByDate(date);
		
		return DomainConverter.convertToDTO(entity);
	}
	
	@Override
	public PeopleCounterDTO getPeopleCounterByTimestamp(DateTimeModel timestamp)
	{
		if(timestamp == null) return null; //TODO ritornare errore da gestire dal controller
		
		String date = Utility.dateToString(timestamp.getDate(), Constants.INPUT_DATE_FORMAT);
		String time = Utility.dateToString(timestamp.getTime(), Constants.INPUT_TIME_FORMAT);
		PeopleCounterEntity entity = peopleCounterDAO.getPeopleCounterByTimestamp(Utility.stringToTimestampUTC(date + time + Utility.getSystemZoneId(), PeopleCounterDTO.TIMESTAMP_FORMAT));
		return DomainConverter.convertToDTO(entity);
	}
	
	@Override
	public List<PeopleCounterDTO> getPeopleCounterByDateInterval(DateIntervalModel dateInterval)
	{
		if(dateInterval == null) dateInterval = new DateIntervalModel();
		else dateInterval.fillIfEmpty();

		if(!dateInterval.areValidInterval()) return null; //TODO ritornare errore da gestire dal controller
		
		List<PeopleCounterEntity> peopleCounterLst = peopleCounterDAO.getPeopleCounterByDateInterval(dateInterval);
		
		if(peopleCounterLst == null) return null;
		
		List<PeopleCounterDTO> peopleCounterDTOLst = new ArrayList<PeopleCounterDTO>();
		for(PeopleCounterEntity entity : peopleCounterLst) 
			peopleCounterDTOLst.add(DomainConverter.convertToDTO(entity));

		return peopleCounterDTOLst;
	}
	
	@Override
	public List<PeopleCounterDTO> getPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval)
	{
		if(dateTimeInterval == null) dateTimeInterval = new DateTimeIntervalModel();
		else dateTimeInterval.fillIfEmpty();
		
		if(!dateTimeInterval.areValidInterval()) return null; //TODO ritornare errore da gestire dal controller
		
		List<PeopleCounterEntity> peopleCounterLst = peopleCounterDAO.getPeopleCounterByDateAndTimeInterval(dateTimeInterval);
		if(peopleCounterLst == null) return null;
		
		List<PeopleCounterDTO> peopleCounterDTOLst = new ArrayList<PeopleCounterDTO>();
		for(PeopleCounterEntity entity : peopleCounterLst) 
			peopleCounterDTOLst.add(DomainConverter.convertToDTO(entity));

		return peopleCounterDTOLst;
	}
	
	@Override
	public List<CounterDateEventModel> getDailyPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval)
	{
		if(dateTimeInterval == null) dateTimeInterval = new DateTimeIntervalModel();
		else dateTimeInterval.fillIfEmpty();
		
		if(!dateTimeInterval.areValidInterval()) return null; //TODO ritornare errore da gestire dal controller
		
		return peopleCounterDAO.getDailyPeopleCounterByDateAndTimeInterval(dateTimeInterval);
	}
	
	@Override
	public List<CounterTimeEventModel> getDailyPeopleCounterDetails(DateTimeIntervalHourDetailModel dateTimeHour) throws ParseException
	{
		if(dateTimeHour == null) dateTimeHour = new DateTimeIntervalHourDetailModel();
		else dateTimeHour.fillIfEmpty();
		
		if(!dateTimeHour.areValid()) return null; //TODO ritornare errore da gestire dal controller
		
		return peopleCounterDAO.getDailyPeopleCounterDetails(dateTimeHour);
	}
	
	@Override
	public List<RegisteredEventModel> getRegisterdDays()
	{
		return peopleCounterDAO.getRegisteredDays();
	}
}
