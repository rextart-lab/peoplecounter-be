package com.rextart.axis.tools.peoplecounter.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import com.rextart.axis.tools.peoplecounter.AxisPeopleCounterBE;
import com.rextart.axis.tools.peoplecounter.domain.dto.PeopleCounterDTO;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalHourDetailModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;
import com.rextart.axis.tools.peoplecounter.service.IPeopleCounterService;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

@RestController
@CrossOrigin
public class PeopleCounterController {
	@Autowired
	IPeopleCounterService peopleCounterService;
	
	@Autowired
	BuildProperties buildProperties;
	
	@Value("${spring.property.AXIS_URL}")
	private String AXIS_URL;

	@Value("${spring.property.AXIS_USER}")
	private String AXIS_USER;

	@Value("${spring.property.AXIS_PWD}")
	private String AXIS_PWD;

//	helper method
	@GetMapping("/")
	public String restMethodList()
	{
		String restList = "";
		for(Method method : getClass().getMethods())
		{
			if(!Modifier.isPublic(method.getModifiers())) continue;
			ServiceDef serviceDef = method.getAnnotation(ServiceDef.class);
			if (serviceDef != null)
			{
				String value = serviceDef.value();
				String methodType = serviceDef.method().name();
				String param = serviceDef.param();
				String descr = serviceDef.desc();

				restList += "<LI>" + "<B>" + "/" + value + "</B>" + " on " + methodType + "</LI>";
				restList += "<UL style=\"list-style-square;\">";
				restList += "<LI>" + "Description: " + descr + "</LI>";
				restList += "<LI>" + "Parameter: " + param + "</LI>";
				restList += "</UL>";
			}
		}

		String title = "<H1>" + buildProperties.getName() + "  v" + buildProperties.getVersion() + "</H1>";
		return title + "<U>REST method:</U>" + "<UL style=\"list-style-circle;\">" + restList + "</UL>";
	}

	@GetMapping("/today")
	@ServiceDef(desc = "Today last event", method = RequestMethod.GET, value = "today")
	public PeopleCounterDTO getTodayEvent()
	{
		PeopleCounterDTO resp = peopleCounterService.getTodayPeopleCounter();
		
//		chiamata diretta verso telecamera
//		RestTemplate restTemplate = new RestTemplate();
//
//		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();        
//		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//		converter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
//		messageConverters.add(converter);
//		restTemplate.setMessageConverters(messageConverters);
//
//		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(AXIS_USER, AXIS_PWD));
//
//		PeopleCounterDTO resp = null;
//		try
//		{
//			resp = restTemplate.getForObject(AXIS_URL, PeopleCounterDTO.class);
//		}
//		catch(ResourceAccessException e)
//		{
//			System.err.println(e.getMessage());
//		}
//
////		per restituire timestamp con formato atteso dal front-end (nel caso di chiamata diretta verso la telecamera) 
//		if(resp != null) resp.setTimestamp(Utility.stringToTimestampUTC(resp.getTimestamp() + Utility.getSystemZoneId(), PeopleCounterDTO.TIMESTAMP_FORMAT).toString());
		
		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		
		System.out.println("<-- REST REQUEST: /today\n--> " + resp);
		
		return resp;
	}

	@GetMapping("/onDate/{date}")
	@ServiceDef(desc = "Last event on specified date", method = RequestMethod.GET, value = "onDate", param = "yyyymmdd")
	public PeopleCounterDTO getEventOnDate(@PathVariable("date") @DateTimeFormat(pattern = Constants.INPUT_DATE_FORMAT) Date date)
	{
		PeopleCounterDTO resp = peopleCounterService.getPeopleCounterByDate(date);

		System.out.println("<-- REST REQUEST: /onDate OF " + date + "\n--> " + resp);

		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return resp;
	} 

	@RequestMapping(value = "/totBetweenDate", method = RequestMethod.POST)
	@ServiceDef(desc = "Tot counter events between specified dates. Default values: start date = epoch, end date = today.", method = RequestMethod.POST, value = "totBetweenDate", param = "{\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}")
	public CounterModel getTotalEventBetweenDate(@RequestBody(required=false) DateIntervalModel dateInterval)
	{
		List<PeopleCounterDTO> dtoLst = peopleCounterService.getPeopleCounterByDateInterval(dateInterval);

		if(dtoLst == null) 
		{
			System.out.println("<-- REST REQUEST: /totBetweenDate OF " + dateInterval + "\n--> " + dtoLst);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		}

		int in = 0;
		int out = 0;
		for(PeopleCounterDTO dto : dtoLst)
		{
			in+=Integer.parseInt(dto.getIn());
			out+=Integer.parseInt(dto.getOut());
		}
		CounterModel res = new CounterModel(in, out);
		System.out.println("<-- REST REQUEST: /totBetweenDate OF " + dateInterval + "\n--> " + res);
		
		return res;
	}

	@RequestMapping(value = "/betweenDate", method = RequestMethod.POST)
	@ServiceDef(desc = "Counter event list between specified dates. Default values: start date = epoch, end date = today.", method = RequestMethod.POST, value = "betweenDate", param = "{\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}")
	public List<PeopleCounterDTO> getEventsBetweenDate(@RequestBody(required=false) DateIntervalModel dateInterval)
	{
		List<PeopleCounterDTO> resp = peopleCounterService.getPeopleCounterByDateInterval(dateInterval);

		System.out.println("<-- REST REQUEST: /betweenDate OF " + dateInterval + "\n--> " + resp);

		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return resp;
	}

	@RequestMapping(value = "/onTimestamp", method = RequestMethod.POST)
	@ServiceDef(desc = "Event on specified timestamp", method = RequestMethod.POST, value = "onTimestamp", param = "{\"date\": \"yyyymmdd\", \"time\": \"hhmmss\"}")
	public PeopleCounterDTO getEventOnTimestamp(@RequestBody DateTimeModel timestamp)
	{
		PeopleCounterDTO resp = peopleCounterService.getPeopleCounterByTimestamp(timestamp);
		
		System.out.println("<-- REST REQUEST: /onTimestamp OF " + timestamp + "\n--> " + resp);

		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return resp;
	}

	@RequestMapping(value = "/betweenDateTime", method = RequestMethod.POST)
	@ServiceDef(desc = "Counter event list between specified dates and times. It return for every day in the interval the two event at the time interval's extremity (otherwise just one if event in that date is the unique). Events are distinct for each camera. Default values: start date = epoch, end date = today, start time = 00:00:00, end time = 23:59:59.",
				method = RequestMethod.POST, value = "betweenDateTime",
				param = "{\"dates:\" {\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}, \"times:\" {\"startTime\": \"hhmmss\", \"endTime\": \"hhmmss\"}}")
	public List<PeopleCounterDTO> getEventsBetweenDateAndTimeInterval(@RequestBody(required=false) DateTimeIntervalModel dateTimeInterval)
	{
		List<PeopleCounterDTO> resp = peopleCounterService.getPeopleCounterByDateAndTimeInterval(dateTimeInterval);
		
		System.out.println("<-- REST REQUEST: /betweenDateTime OF " + dateTimeInterval + "\n--> " + resp);

		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return resp;
	}

	@RequestMapping(value = "/dailyBetweenDateTime", method = RequestMethod.POST)
	@ServiceDef(desc = "Counter event list between specified dates and times. It return for every day one event. Default values: start date = epoch, end date = today, start time = 00:00:00, end time = 23:59:59.",
				method = RequestMethod.POST,
				value = "dailyBetweenDateTime",
				param = "{\"dates:\" {\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}, \"times:\" {\"startTime\": \"hhmmss\", \"endTime\": \"hhmmss\"}}")
	public List<CounterDateEventModel> getDailyEventsBetweenDateAndTimeInterval(@RequestBody(required=false) DateTimeIntervalModel dateTimeInterval)
	{
		List<CounterDateEventModel> resp = peopleCounterService.getDailyPeopleCounterByDateAndTimeInterval(dateTimeInterval);

		System.out.println("<-- REST REQUEST: /dailyBetweenDateTime OF " + dateTimeInterval + "\n--> " + resp);

		if(resp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return resp;
	}

	@RequestMapping(value = "/averageBetweenDateTime", method = RequestMethod.POST)
	@ServiceDef(desc = "Average counter events between specified dates and times. Default values: start date = epoch, end date = today, start time = 00:00:00, end time = 23:59:59.",
				method = RequestMethod.POST,
				value = "averageBetweenDateTime",
				param = "{\"dates:\" {\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}, \"times:\" {\"startTime\": \"hhmmss\", \"endTime\": \"hhmmss\"}}")
	public CounterModel getAverageEventsBetweenDateAndTimeInterval(@RequestBody(required=false) DateTimeIntervalModel dateTimeInterval)
	{
		List<CounterDateEventModel> resp = peopleCounterService.getDailyPeopleCounterByDateAndTimeInterval(dateTimeInterval);

		if(resp == null)
		{
			System.out.println("<-- REST REQUEST: /averageBetweenDateTime OF " + dateTimeInterval + "\n--> " + resp);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		}

		int in = 0;
		int out = 0;
		for(CounterDateEventModel r : resp)
		{
			in+=r.getInEvent();
			out+=r.getOutEvent();
		}

		int averageIn = resp.size() > 0 ? in/resp.size() : 0;
		int averageOut = resp.size() > 0 ? out/resp.size() : 0;
		CounterModel res = new CounterModel(averageIn, averageOut);
		
		System.out.println("<-- REST REQUEST: /averageBetweenDateTime OF " + dateTimeInterval + "\n--> " + res);
		return res;
	}

	@RequestMapping(value = "/averageBetweenDate", method = RequestMethod.POST)
	@ServiceDef(desc = "Average counter events between specified dates. Default values: start date = epoch, end date = today.", method = RequestMethod.POST, param = "{\"startDate\": \"yyyymmdd\", \"endDate\": \"yyyymmdd\"}", value = "averageBetweenDate")     
	public CounterModel getAverageEventBetweenDate(@RequestBody(required=false) DateIntervalModel dateInterval)
	{
		List<PeopleCounterDTO> dtoLst = peopleCounterService.getPeopleCounterByDateInterval(dateInterval);

		if(dtoLst == null) 
		{
			System.out.println("<-- REST REQUEST: /averageBetweenDate OF " + dateInterval + "\n--> " + dtoLst);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		}

		int in = 0;
		int out = 0;
		for(PeopleCounterDTO dto : dtoLst)
		{
			in+=Integer.parseInt(dto.getIn());
			out+=Integer.parseInt(dto.getOut());
		}

		int averageIn = dtoLst.size() > 0 ? in/dtoLst.size() : 0;
		int averageOut = dtoLst.size() > 0 ? out/dtoLst.size() : 0;
		CounterModel res = new CounterModel(averageIn, averageOut);
		
		System.out.println("<-- REST REQUEST: /averageBetweenDate OF " + dateInterval + "\n--> " + res);
		
		return res;
	}
	
	@RequestMapping(value = "/dailyDetails", method = RequestMethod.POST)
	@ServiceDef(desc = "Counter event list between specified dates and times at minute detail specified. Default values: date = epoch, min = 60, start time = 00:00:00, end time = 23:59:59.", method = RequestMethod.POST, param = "{\"date\": \"yyyymmdd\", \"min\": mm, \"times:\" {\"startTime\": \"hhmmss\", \"endTime\": \"hhmmss\"}}", value = "dailyDetails")
	public List<CounterTimeEventModel> getDetailedDailyEvent(@RequestBody(required=false) DateTimeIntervalHourDetailModel dateTimeMinute) throws ParseException
	{				
		List<CounterTimeEventModel> res = peopleCounterService.getDailyPeopleCounterDetails(dateTimeMinute);
		
		System.out.println("<-- REST REQUEST: /dailyDetails OF " + dateTimeMinute + "\n--> " + res);

		if(res == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return res;
	}
	
	@RequestMapping(value = "/registeredDays", method = RequestMethod.GET)
	@ServiceDef(desc = "List of registerd days with event count.", method = RequestMethod.GET, value = "registeredDays")     
	public List<RegisteredEventModel> getRegisteredDays()
	{
		List<RegisteredEventModel> res = peopleCounterService.getRegisterdDays();
		
		System.out.println("<-- REST REQUEST: /registeredDays\n--> " + res);

		if(res == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
		return res;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(value = ElementType.METHOD)
	public @interface ServiceDef 
	{
		public String value();
		public RequestMethod method();
		public String param() default "-";
		public String desc();
	}
}
