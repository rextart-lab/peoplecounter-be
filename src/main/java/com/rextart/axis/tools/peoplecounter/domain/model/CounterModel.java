package com.rextart.axis.tools.peoplecounter.domain.model;

import java.io.Serializable;

public class CounterModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer in_event;
	private Integer out_event;

	public CounterModel() {}

	public CounterModel(Integer in_event, Integer out_event)
	{
		this.in_event = in_event;
		this.out_event = out_event;
	}
	
	public Integer getInEvent() {
		return in_event;
	}
	public void setInEvent(Integer in_event) {
		this.in_event = in_event;
	}
	public Integer getOutEvent() {
		return out_event;
	}
	public void setOut(Integer out_event) {
		this.out_event = out_event;
	}

	@Override
	public String toString()
	{
		return "CounterModel [in_event=" + in_event + ", out_event=" + out_event + "]";
	}
}
