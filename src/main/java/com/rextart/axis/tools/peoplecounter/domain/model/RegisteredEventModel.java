package com.rextart.axis.tools.peoplecounter.domain.model;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class RegisteredEventModel implements Serializable
{
	private static final long serialVersionUID = 1L;

	@JsonFormat(pattern=Constants.OUTPUT_DATE_FORMAT)
	private Date date;
	private Integer count;
	
	public RegisteredEventModel(Date date, Integer count)
	{
		this.date = date;
		this.count = count;
	}
	
	public RegisteredEventModel() {}
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	public Integer getCount()
	{
		return count;
	}
	
	public void setCount(Integer count)
	{
		this.count = count;
	}

	@Override
	public String toString()
	{
		String dateStr = date == null ? null : Utility.dateToString(date, Constants.OUTPUT_DATE_FORMAT);
		return "RegisteredEventModel [date=" + dateStr + ", count=" + count + "]";
	}
}
