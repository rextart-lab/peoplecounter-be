package com.rextart.axis.tools.peoplecounter.domain.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class DateIntervalModel 
{
	@JsonFormat(pattern=Constants.INPUT_DATE_FORMAT)
	private Date startDate = new Date(0L);
	
	@JsonFormat(pattern=Constants.INPUT_DATE_FORMAT)
	private Date endDate = new Date();
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public void fillIfEmpty()
	{
		if(startDate == null) startDate = new Date(0L);
		if(endDate == null) endDate = new Date();
	}
	
	public boolean areValidInterval()
	{
		return startDate != null && endDate != null && startDate.compareTo(endDate) <= 0;
	}
	
	@Override
	public String toString()
	{
		String startDateStr = startDate == null ? null : Utility.dateToString(startDate, Constants.INPUT_DATE_FORMAT);
		String endDateStr = endDate == null ? null : Utility.dateToString(endDate, Constants.INPUT_DATE_FORMAT);
		return "DateIntervalModel [startDate=" + startDateStr + ", endDate=" + endDateStr + "]";
	}
}
