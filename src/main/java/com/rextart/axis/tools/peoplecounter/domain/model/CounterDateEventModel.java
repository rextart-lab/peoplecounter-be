package com.rextart.axis.tools.peoplecounter.domain.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class CounterDateEventModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonFormat(pattern=Constants.OUTPUT_DATE_FORMAT)
	private Date date;
	private Integer in_event;
	private Integer out_event;
	
	public CounterDateEventModel() {}
	
	public CounterDateEventModel(Date date, Integer in_event, Integer out_event)
	{
		this.date = date;
		this.in_event = in_event;
		this.out_event = out_event;
	}
	
	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public Integer getInEvent()
	{
		return in_event;
	}

	public void setInEvent(Integer in_event)
	{
		this.in_event = in_event;
	}

	public Integer getOutEvent()
	{
		return out_event;
	}

	public void setOutEvent(Integer out_event)
	{
		this.out_event = out_event;
	}

	@Override
	public String toString()
	{
		String dateStr = date == null ? null : Utility.dateToString(date, Constants.OUTPUT_DATE_FORMAT);
		return "CounterDateEventModel [date=" + dateStr + ", in_event=" + in_event + ", out_event=" + out_event + "]";
	}
}
