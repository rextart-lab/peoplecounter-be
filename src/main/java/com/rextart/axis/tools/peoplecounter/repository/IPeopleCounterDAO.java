package com.rextart.axis.tools.peoplecounter.repository;

import java.text.ParseException;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import com.rextart.axis.tools.peoplecounter.domain.entity.PeopleCounterEntity;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalHourDetailModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;

public interface IPeopleCounterDAO
{

	List<PeopleCounterEntity> getAllPeopleCounter();

	PeopleCounterEntity getPeopleCounterById(Long counterId);

	void addPeopleCounter(PeopleCounterEntity counter);

	boolean PeopleCounterExists(String serial, String name);

	PeopleCounterEntity getPeopleCounterByDate(Date inDate);

	PeopleCounterEntity getPeopleCounterByTimestamp(OffsetDateTime inTimestamp);

	List<PeopleCounterEntity> getPeopleCounterByDateInterval(DateIntervalModel dateInterval);

	List<PeopleCounterEntity> getPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel ddateTimeInterval);

	List<CounterDateEventModel> getDailyPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval);

	List<RegisteredEventModel> getRegisteredDays();

	List<CounterTimeEventModel> getDailyPeopleCounterDetails(DateTimeIntervalHourDetailModel dateHour)
			throws ParseException;
}
