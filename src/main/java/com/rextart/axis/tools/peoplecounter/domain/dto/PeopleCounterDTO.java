package com.rextart.axis.tools.peoplecounter.domain.dto;

import java.io.Serializable;

public class PeopleCounterDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String serial;
	private String name;
	private String timestamp;
	private String in;
	private String out;
	public static final String TIMESTAMP_FORMAT = "yyyyMMddHHmmssXXX";

	public PeopleCounterDTO() {}

	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getIn() {
		return in;
	}
	
	public void setIn(String in) {
		this.in = in;
	}
	public String getOut() {
		return out;
	}
	public void setOut(String out) {
		this.out = out;
	}

	@Override
	public String toString()
	{
		return "PeopleCounterDTO [serial=" + serial + ", name=" + name + ", timestamp=" + timestamp + ", in=" + in
				+ ", out=" + out + "]";
	}
}