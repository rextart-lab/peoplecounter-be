package com.rextart.axis.tools.peoplecounter.domain.model;

public class DateTimeIntervalModel
{
	private DateIntervalModel dates = new DateIntervalModel();
	private TimeIntervalModel times = new TimeIntervalModel();
	
	public DateIntervalModel getDates()
	{
		return dates;
	}
	
	public void setDates(DateIntervalModel dates)
	{
		this.dates = dates;
	}
	
	public TimeIntervalModel getTimes()
	{
		return times;
	}
	
	public void setTimes(TimeIntervalModel times)
	{
		this.times = times;
	}
	
	public boolean areValidInterval()
	{
		return dates != null && times != null && dates.areValidInterval() && times.areValidInterval();
	}
	
	public void fillIfEmpty()
	{
		if(dates != null) dates.fillIfEmpty();
		if(times != null) times.fillIfEmpty();
	}

	@Override
	public String toString()
	{
		return "DateTimeIntervalModel [dates=" + dates + ", times=" + times + "]";
	}
}
