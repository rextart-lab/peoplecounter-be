package com.rextart.axis.tools.peoplecounter.domain.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class TimeIntervalModel {
	@JsonFormat(pattern=Constants.INPUT_TIME_FORMAT, timezone=JsonFormat.DEFAULT_TIMEZONE)
	private Date startTime = Utility.stringToDate("000000", Constants.INPUT_TIME_FORMAT);
	
	@JsonFormat(pattern=Constants.INPUT_TIME_FORMAT, timezone=JsonFormat.DEFAULT_TIMEZONE)
	private Date endTime = Utility.stringToDate("235959", Constants.INPUT_TIME_FORMAT);

	public Date getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public boolean areValidInterval()
	{
		return startTime != null && endTime != null && startTime.compareTo(endTime) < 0;
	}
	
	public void fillIfEmpty()
	{
		if(startTime == null) startTime = Utility.stringToDate("000000", Constants.INPUT_TIME_FORMAT);
		if(endTime == null) endTime = Utility.stringToDate("235959", Constants.INPUT_TIME_FORMAT);
	}
	
	@Override
	public String toString()
	{
		String startTimeStr = startTime == null ? null : Utility.dateToString(startTime, Constants.INPUT_TIME_FORMAT);
		String endTimeStr = endTime == null ? null : Utility.dateToString(endTime, Constants.INPUT_TIME_FORMAT);
		return "TimeIntervalModel [startTime=" + startTimeStr + ", endTime=" + endTimeStr + "]";
	}
}
