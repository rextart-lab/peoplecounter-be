package com.rextart.axis.tools.peoplecounter.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.rextart.axis.tools.peoplecounter.domain.dto.PeopleCounterDTO;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalHourDetailModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;

public interface IPeopleCounterService {
	
	boolean addPeopleCounter(PeopleCounterDTO counter);
	void updatePeopleCounter(PeopleCounterDTO counter);
    void deletePeopleCounter(int id);
    boolean peopleCounterExists(String serial, String name);
    PeopleCounterDTO getTodayPeopleCounter();
    PeopleCounterDTO getPeopleCounterByDate(Date date);
    PeopleCounterDTO getPeopleCounterByTimestamp(DateTimeModel timestamp);
    List<PeopleCounterDTO> getPeopleCounterByDateInterval(DateIntervalModel dateInterval);
	List<PeopleCounterDTO> getPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval);
	List<CounterDateEventModel> getDailyPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval);
	List<RegisteredEventModel> getRegisterdDays();
	List<CounterTimeEventModel> getDailyPeopleCounterDetails(DateTimeIntervalHourDetailModel dateHour) throws ParseException;
}
