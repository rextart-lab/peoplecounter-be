package com.rextart.axis.tools.peoplecounter.domain.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;


@SqlResultSetMapping(name="counterDateEvent",
classes = {
		@ConstructorResult(
				targetClass = CounterDateEventModel.class,
				columns = {
						@ColumnResult(name = "date", type = Date.class),
						@ColumnResult(name = "in_event", type = Integer.class),
						@ColumnResult(name = "out_event", type = Integer.class)
				})
})

@SqlResultSetMapping(name="counterTimeEvent",
classes = {
		@ConstructorResult(
				targetClass = CounterTimeEventModel.class,
				columns = {
						@ColumnResult(name = "time", type = Date.class),
						@ColumnResult(name = "in_event", type = Integer.class),
						@ColumnResult(name = "out_event", type = Integer.class)
				})
})

@SqlResultSetMapping(name="registeredEvent",
classes = {
		@ConstructorResult(
				targetClass = RegisteredEventModel.class,
				columns = {
						@ColumnResult(name = "date", type = Date.class),
						@ColumnResult(name = "count", type = Integer.class)
				})
})

@Entity
@Table(name = "people_counter")
public class PeopleCounterEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "serial")
	private String serial;

	@Column(name = "name")
	private String name;

	/*	
	 * JPA ignora il campo così da lasciare che il db lo valorizzi con quello previsto per default
	 * in questo modo l'onere di dover settare correttamente l'orario ed il timezone rimane al db 
	 */
	@Column(name = "time_control", insertable = false)
	private OffsetDateTime time_control;

	@Column(name = "in_event")
	private Integer in_event;

	@Column(name = "out_event")
	private Integer out_event;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIn_event() {
		return in_event;
	}	
	public void setIn_event(Integer in_event) {
		this.in_event = in_event;
	}	
	public Integer getOut_event() {
		return out_event;
	}	
	public void setOut_event(Integer out_event) {
		this.out_event = out_event;
	}
	public OffsetDateTime getTime_control()
	{
		return time_control;
	}
	public void setTime_control(OffsetDateTime time_control)
	{
		this.time_control = time_control;
	}

	@Override
	public String toString()
	{
		return "PeopleCounterEntity [id=" + id + ", serial=" + serial + ", name=" + name + ", time_control="
				+ time_control + ", in_event=" + in_event + ", out_event=" + out_event + "]";
	}
}
