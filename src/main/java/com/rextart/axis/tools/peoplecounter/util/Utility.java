package com.rextart.axis.tools.peoplecounter.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Utility 
{
	public static String getSystemZoneId()
	{
		OffsetDateTime now = OffsetDateTime.now();
		return now.getOffset().toString();
	}
	
	public static OffsetDateTime stringToTimestampUTC(String stringTimestamp, String timestampFormat)
	{
		DateTimeFormatter format = DateTimeFormatter.ofPattern(timestampFormat);
		OffsetDateTime timestamp = OffsetDateTime.parse(stringTimestamp, format);
		return timestamp;
	}
	
	public static String dateToString(Date date, String dateFormat)
	{
		DateFormat format = new SimpleDateFormat(dateFormat);  
		return format.format(date);  
	}
	
	public static Date stringToDate(String stringDate, String dateFormat)
	{
		SimpleDateFormat date = new SimpleDateFormat(dateFormat);
		try {
			return date.parse(stringDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static boolean isEmptyOrNull(List<?> lst)
	{
		return lst == null || lst.isEmpty();
	}
	
	public static Date addToDate(Date date, int field, int value)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, value);
		return cal.getTime();
	}
	
	public static Date addMinuteToDate(Date date, int min)
	{
		return addToDate(date, Calendar.MINUTE, min);
	}
}
