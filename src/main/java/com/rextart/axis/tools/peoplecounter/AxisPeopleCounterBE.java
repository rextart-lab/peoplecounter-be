package com.rextart.axis.tools.peoplecounter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.rextart.axis.tools.peoplecounter.domain.dto.PeopleCounterDTO;
import com.rextart.axis.tools.peoplecounter.service.IPeopleCounterService;

// TODO implementare sistema di logging su file
@SpringBootApplication
@EnableScheduling
public class AxisPeopleCounterBE
{

	@Value("${spring.property.AXIS_URL}")
	private String AXIS_URL;

	@Value("${spring.property.AXIS_USER}")
	private String AXIS_USER;

	@Value("${spring.property.AXIS_PWD}")
	private String AXIS_PWD;

	@Value("${spring.property.AXIS_REFRESH_TIME}")
	private String AXIS_REFRESH_TIMED;

	@Autowired
	private IPeopleCounterService pcService;

	public static void main(String[] args)
	{
		SpringApplication.run(AxisPeopleCounterBE.class, args);
	}

	@Component
	public class AxisScheduledTasks
	{

		@Scheduled(cron = "${spring.property.AXIS_REFRESH_TIME}")
		public void registerCurrentCount()
		{
			RestTemplate restTemplate = new RestTemplate();

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
			converter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
			messageConverters.add(converter);
			restTemplate.setMessageConverters(messageConverters);

			restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(AXIS_USER, AXIS_PWD));

			try
			{
				PeopleCounterDTO pcDTO = restTemplate.getForObject(AXIS_URL, PeopleCounterDTO.class);

				if(pcDTO != null)
				{
					System.out.println("----> writing to db: " + pcDTO);
					pcService.addPeopleCounter(pcDTO);
				}
				else
				{
					System.out.println("DTO null");
				}
			}
			catch(ResourceAccessException e)
			{
				System.err.println(e.getMessage());
			}
		}
	}
}