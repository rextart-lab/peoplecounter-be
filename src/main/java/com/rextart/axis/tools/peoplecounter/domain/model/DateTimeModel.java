package com.rextart.axis.tools.peoplecounter.domain.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class DateTimeModel {
	
	@JsonFormat(pattern=Constants.INPUT_DATE_FORMAT)
	private Date date;
	
	@JsonFormat(pattern=Constants.INPUT_TIME_FORMAT, timezone=JsonFormat.DEFAULT_TIMEZONE)
	private Date time;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	@Override
	public String toString()
	{
		String dateStr = date == null ? null : Utility.dateToString(date, Constants.INPUT_DATE_FORMAT);
		String timeStr = time == null ? null : Utility.dateToString(time, Constants.INPUT_TIME_FORMAT);
		return "DateTimeModel [date=" + dateStr + ", time=" + timeStr + "]";
	}
}
