package com.rextart.axis.tools.peoplecounter.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.axis.tools.peoplecounter.domain.entity.PeopleCounterEntity;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterDateEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.CounterTimeEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalHourDetailModel;
import com.rextart.axis.tools.peoplecounter.domain.model.DateTimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.domain.model.RegisteredEventModel;
import com.rextart.axis.tools.peoplecounter.domain.model.TimeIntervalModel;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

@Transactional
@Repository
public class PeopleCounterDAO implements IPeopleCounterDAO
{

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<PeopleCounterEntity> getAllPeopleCounter()
	{
		String hql = "FROM PeopleCounterEntity as pc ORDER BY pc.id";
		return (List<PeopleCounterEntity>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public PeopleCounterEntity getPeopleCounterById(Long counterId)
	{
		return entityManager.find(PeopleCounterEntity.class, counterId);
	}

	@Override
	public void addPeopleCounter(PeopleCounterEntity counter)
	{
		entityManager.persist(counter);
	}

	@Override
	public boolean PeopleCounterExists(String serial, String name)
	{
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PeopleCounterEntity getPeopleCounterByDate(Date inDate)
	{
		String query = "select * from people_counter where date(time_control) = :date order by time_control desc limit 1;";
		Query q = entityManager.createNativeQuery(query, PeopleCounterEntity.class);
		q.setParameter("date", Utility.dateToString(inDate, Constants.INPUT_DATE_FORMAT));

		List<PeopleCounterEntity> results = q.getResultList();
		if(Utility.isEmptyOrNull(results)) return null;

		return results.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PeopleCounterEntity> getPeopleCounterByDateInterval(DateIntervalModel dateInterval)
	{
		String query = "" +
				"SELECT  " +
				"    * " +
				"FROM " +
				"    ( " +
				"	    SELECT  " +
				"	        MAX(id) max_id " +
				"	    FROM " +
				"	        people_counter " +
				"	    WHERE " +
				"	        DATE(time_control) >= date(:startDate) " +
				"	        AND DATE(time_control) <= date(:endDate) " +
				"	    GROUP BY DATE(time_control) " +
				"	) max_pc, " +
				"    people_counter " +
				"WHERE " +
				"    max_pc.max_id = people_counter.id " +
				"order by people_counter.time_control;";

		Query q = entityManager.createNativeQuery(query, PeopleCounterEntity.class);
		q.setParameter("startDate", Utility.dateToString(dateInterval.getStartDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("endDate", Utility.dateToString(dateInterval.getEndDate(), Constants.INPUT_DATE_FORMAT));

		List<PeopleCounterEntity> results = q.getResultList();
		if(Utility.isEmptyOrNull(results)) return null;

		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PeopleCounterEntity getPeopleCounterByTimestamp(OffsetDateTime inTimestamp)
	{
		String query = "select * from people_counter where time_control = :timestamp limit 1;";
		Query q = entityManager.createNativeQuery(query, PeopleCounterEntity.class);
		q.setParameter("timestamp", inTimestamp);

		List<PeopleCounterEntity> results = q.getResultList();
		if(Utility.isEmptyOrNull(results)) return null;

		return results.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CounterDateEventModel> getDailyPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval)
	{
		DateIntervalModel dateInterval = dateTimeInterval.getDates();
		TimeIntervalModel timeInterval = dateTimeInterval.getTimes();

		//		TODO ottimizzare query
		/*
		 * Nel caso di telecamere diverse che registrano nello stesso istante la query
		 * restituisce più di 2 record per giorno, per questo è stata aggiunta la
		 * funzione sum Tra gli eventi non viene tenuto in considerazione quello delle
		 * 00:00 in quanto non mostra i valori resettati ma il totale del giorno
		 * precedente.
		 */
		String query = "select " +
				"    date(last_ev.time_control) date, " +
				"	 sum(if(first_ev.in_event is not null, last_ev.in_event - first_ev.in_event, last_ev.in_event)) as in_event, " +
				"    sum(if(first_ev.out_event is not null, last_ev.out_event - first_ev.out_event, last_ev.out_event)) as out_event " +
				"from " +
				"( " +
				"	select ev.*, time_control, in_event, out_event " +
				"	from " +
				"	( " +
				"		SELECT MAX(id) id, serial ser " +
				"		FROM people_counter " +
				"		WHERE " +
				"			date(time_control) >= date(:startDate) " +
				"            and date(time_control) <= date(:endDate) " +
				"			and time(time_control) >= time(:startTime) " +
				"			and time(time_control) <= time(:endTime) " +
				"			and time(time_control) <> time('00:00:00')" +
				"		GROUP BY DATE(time_control), serial " +
				"	) ev, " +
				"	people_counter " +
				"	where people_counter.id = ev.id " +
				") last_ev " +
				"left join " +
				"( " +
				"	select ev.*, time_control, in_event, out_event " +
				"	from " +
				"	( " +
				"		SELECT MIN(id) id, serial ser " +
				"		FROM people_counter " +
				"		WHERE " +
				"			date(time_control) >= date(:startDate) " +
				"			and date(time_control) <= date(:endDate) " +
				"			and time(time_control) >= time(:startTime) " +
				"			and time(time_control) <= time(:endTime) " +
				"			and time(time_control) <> time('00:00:00')" +
				"		GROUP BY DATE(time_control), serial " +
				"	) ev, " +
				"	people_counter " +
				"	where people_counter.id = ev.id " +
				") first_ev " +
				"on date(last_ev.time_control) = date(first_ev.time_control) and first_ev.ser = last_ev.ser and first_ev.id <> last_ev.id " +
				"group by date(last_ev.time_control) " +
				"order by date;";

		Query q = entityManager.createNativeQuery(query, "counterDateEvent");
		q.setParameter("startDate", Utility.dateToString(dateInterval.getStartDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("endDate", Utility.dateToString(dateInterval.getEndDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("startTime", Utility.dateToString(timeInterval.getStartTime(), Constants.INPUT_TIME_FORMAT));
		q.setParameter("endTime", Utility.dateToString(timeInterval.getEndTime(), Constants.INPUT_TIME_FORMAT));

		List<CounterDateEventModel> results = q.getResultList();

		if(Utility.isEmptyOrNull(results)) return null;

		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PeopleCounterEntity> getPeopleCounterByDateAndTimeInterval(DateTimeIntervalModel dateTimeInterval)
	{
		DateIntervalModel dateInterval = dateTimeInterval.getDates();
		TimeIntervalModel timeInterval = dateTimeInterval.getTimes();

		//		TODO ottimizzare query
		/*
		 * La query ritorna tutti gli eventi distinguendo per tipologia di telecamera (identificata dall'attributo serial),
		 * per ogni giorno può quindi generare più di 2 record.
		 * Nella differenza tra gli eventi in ed out max e min viene tenuto in
		 * considerazione che alle 00:00 di ogni giorno il contatore prima di resettarsi
		 * mostra il totale del giorno precedente. Perciò nel caso in cui l'evento
		 * appartenente all'intervallo precedente abbia un valore < non viene
		 * considerato nella differenza.
		 */
		String query = "SELECT people_counter.* " +
				"FROM  " +
				"    people_counter, " +
				"    ( " +
				"		SELECT  " +
				"			MAX(id) id " +
				"		FROM " +
				"			people_counter " +
				"		WHERE " +
				"			DATE(time_control) >= date(:startDate) " +
				"				AND DATE(time_control) <= date(:endDate) " +
				"				AND TIME(time_control) >= time(:startTime) " +
				"				AND TIME(time_control) <= time(:endTime) " +
				"		GROUP BY DATE(time_control) , serial " +
				"		union " +
				"		SELECT " +
				"			min(id) id " +
				"		FROM " +
				"			people_counter " +
				"		WHERE " +
				"			DATE(time_control) >= date(:startDate) " +
				"				AND DATE(time_control) <= date(:endDate) " +
				"				AND TIME(time_control) >= time(:startTime) " +
				"				AND TIME(time_control) <= time(:endTime) " +
				"		GROUP BY DATE(time_control) , serial " +
				"	) ev " +
				"where people_counter.id = ev.id " +
				"order by people_counter.time_control;";

		Query q = entityManager.createNativeQuery(query, PeopleCounterEntity.class);
		q.setParameter("startDate", Utility.dateToString(dateInterval.getStartDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("endDate", Utility.dateToString(dateInterval.getEndDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("startTime", Utility.dateToString(timeInterval.getStartTime(), Constants.INPUT_TIME_FORMAT));
		q.setParameter("endTime", Utility.dateToString(timeInterval.getEndTime(), Constants.INPUT_TIME_FORMAT));

		List<PeopleCounterEntity> results = q.getResultList();

		if(Utility.isEmptyOrNull(results)) return null;

		return results;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CounterTimeEventModel> getDailyPeopleCounterDetails(DateTimeIntervalHourDetailModel dateTimeHour)
			throws ParseException
	{
		Date startTime = dateTimeHour.getTimes().getStartTime(); //minimo = 00:00
		Date endTime = dateTimeHour.getTimes().getEndTime(); //massimo = 23:59

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

		/*
		 * OLD METHOD
		 * String times = " ( ";
		 * 
		 * long diff = endTime.getTime() - startTime.getTime(); long diffHours = diff /
		 * (60 * 60 * 1000); Date lastTime = null;
		 * 
		 * int c = 0; for(int i=0; i<24; i++) { for(int j=0; j<60;
		 * j+=dateTimeHour.getMin()) { Date time = sdf.parse(i + ":" + j);
		 * if(time.before(startTime) || time.after(endTime)) continue;
		 * 
		 * c++; // il primo evento corrisponde a quello con time = 00:00 + min e termina
		 * con 23:59 if(c == 1) continue;
		 * 
		 * lastTime = time; times += "select \'" + i + ":" + j + "\' as t "; if(c <=
		 * (60/dateTimeHour.getMin() * diffHours)) times += "union "; } }
		 * 
		 * if(!lastTime.equals(endTime)) times += "union select \'" +
		 * sdf.format(endTime) + "\' as t";
		 * 
		 * times += " ) ";
		 */

		String times = " ( ";

		Date lastTime = startTime;
		while(Utility.addMinuteToDate(lastTime, dateTimeHour.getMin()).before(endTime))
		{
			lastTime = Utility.addMinuteToDate(lastTime, dateTimeHour.getMin());
			times += "select \'" + sdf.format(lastTime) + "\' as t union ";
		}

		times += "select \'" + sdf.format(endTime) + "\' as t";
		times += " ) ";

		//		TODO ottimizzare query
		/*
		 * La query ritorna gli eventi secondo l'intervallo (min 1 max 60) a partire
		 * dalle 00:00. Gli eventi per ogni intervallo sono calcolati sui dati
		 * registrati tra l'intervallo i-esimo - 1 e l'i-esimo, tenendo in
		 * considerazione delle possibili diverse telecamere.
		 * Nella differenza tra gli eventi in ed out max e min viene tenuto in considerazione che alle 00:00 di ogni giorno il contatore prima di resettarsi mostra
		 * il totale del giorno precedente. Perciò nel caso in cui l'evento appartenente all'intervallo precedente abbia un valore < non viene considerato nella differenza.
		 */
		String query = "" +
				"select " +
				"	time(t) time, " +
				"	sum(if(mi_in_event is not null and ma_in_event >= mi_in_event, ma_in_event - mi_in_event, ma_in_event)) as in_event, " +
				"	sum(if(mi_out_event is not null and ma_out_event >= mi_out_event, ma_out_event - mi_out_event, ma_out_event)) as out_event " +
				"from " +
				"( " +
				"	select ma.t t, ma.in_event ma_in_event, ma.out_event ma_out_event, mi.in_event mi_in_event, mi.out_event mi_out_event " +
				"   from " +
				"    ( " +
				"		select ma.t, people_counter.id id, people_counter.serial serial, people_counter.in_event in_event, people_counter.out_event out_event " +
				"       from " +
				"		( " +
				"			select max(id) id, time(t) t " +
				"			from " + times + " times " +
				"			left join " +
				"			( " +
				"				SELECT * " +
				"				FROM people_counter " +
				"				WHERE " +
				"					DATE(time_control) = DATE(:date) " +
				"			) pc " +
				"			on " +
				"				time(pc.time_control) >= time(times.t) - interval :min minute " +
				"				and time(pc.time_control) < time(times.t) " +
				"			group by time(t), serial " +
				"        ) ma " +
				"        left join people_counter " +
				"        on ma.id = people_counter.id " +
				"	) ma " +
				"    left join " +
				"    ( " +
				"		select mi.t, people_counter.id id, people_counter.serial serial, people_counter.in_event in_event, people_counter.out_event out_event " +
				"        from " +
				"        ( " +
				"			select min(id) id, time(t) t " +
				"			from " + times + " times " +
				"			left join " +
				"			( " +
				"				SELECT * " +
				"				FROM people_counter " +
				"				WHERE " +
				"					DATE(time_control) = DATE(:date) " +
				"			) pc " +
				"			on " +
				"				time(pc.time_control) >= time(times.t) - interval :min minute " +
				"				and time(pc.time_control) < time(times.t) " +
				"			group by time(t), serial " +
				"		) mi, people_counter " +
				"        where mi.id = people_counter.id " +
				"    ) mi " +
				"    on mi.t = ma.t and mi.serial = ma.serial and mi.id <> ma.id " +
				") lst " +
				"group by time(lst.t) " +
				"order by time(lst.t);";

		Query q = entityManager.createNativeQuery(query, "counterTimeEvent");
		q.setParameter("date", Utility.dateToString(dateTimeHour.getDate(), Constants.INPUT_DATE_FORMAT));
		q.setParameter("min", dateTimeHour.getMin());

		List<CounterTimeEventModel> results = q.getResultList();

		if(Utility.isEmptyOrNull(results)) return null;

		return results;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RegisteredEventModel> getRegisteredDays()
	{
		String query = "SELECT distinct(date(time_control)) date, count(*) count from people_counter group by date(time_control) order by 1;";
		Query q = entityManager.createNativeQuery(query, "registeredEvent");
		List<RegisteredEventModel> results = q.getResultList();

		if(Utility.isEmptyOrNull(results)) return null;

		return results;
	}
}
