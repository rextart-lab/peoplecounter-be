package com.rextart.axis.tools.peoplecounter.util;

public final class Constants
{
	public static final String INPUT_DATE_FORMAT = "yyyyMMdd";
	public static final String INPUT_TIME_FORMAT = "HHmmss";
	public static final String OUTPUT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String OUTPUT_TIME_FORMAT = "HH:mm:ss";
}
