package com.rextart.axis.tools.peoplecounter.domain.model;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.rextart.axis.tools.peoplecounter.util.Constants;
import com.rextart.axis.tools.peoplecounter.util.Utility;

public class CounterTimeEventModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonFormat(pattern=Constants.OUTPUT_TIME_FORMAT)
	private Date time;
	private Integer in_event;
	private Integer out_event;
	
	public CounterTimeEventModel() {}
	
	public CounterTimeEventModel(Date time, Integer in_event, Integer out_event)
	{
		this.time = time;
		this.in_event = in_event;
		this.out_event = out_event;
	}
	
	public Date getTime()
	{
		return time;
	}

	public void setTime(Date time)
	{
		this.time = time;
	}

	public Integer getInEvent()
	{
		return in_event;
	}

	public void setInEvent(Integer in_event)
	{
		this.in_event = in_event;
	}

	public Integer getOutEvent()
	{
		return out_event;
	}

	public void setOutEvent(Integer out_event)
	{
		this.out_event = out_event;
	}

	@Override
	public String toString()
	{
		String timeStr = time == null ? null : Utility.dateToString(time, Constants.OUTPUT_TIME_FORMAT);
		return "CounterTimeEventModel [time=" + timeStr + ", in_event=" + in_event + ", out_event=" + out_event + "]";
	}
}
