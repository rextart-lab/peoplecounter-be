package com.rextart.axis.tools.peoplecounter.util;

import com.rextart.axis.tools.peoplecounter.domain.dto.PeopleCounterDTO;
import com.rextart.axis.tools.peoplecounter.domain.entity.PeopleCounterEntity;

public class DomainConverter {
	
	public static PeopleCounterDTO convertToDTO(PeopleCounterEntity entity)
	{
		if(entity == null) return null;
		
		PeopleCounterDTO dto = new PeopleCounterDTO();
		dto.setName(entity.getName());
		dto.setSerial(entity.getSerial());
		dto.setIn(entity.getIn_event().toString());
		dto.setOut(entity.getOut_event().toString());
		dto.setTimestamp(entity.getTime_control().toString());
		
		return dto;
	}
	
	public static PeopleCounterEntity convertToEntity(PeopleCounterDTO dto)
	{
		if(dto == null) return null;
		
    	PeopleCounterEntity entity = new PeopleCounterEntity();
 
    	entity.setName(dto.getName());
    	entity.setSerial(dto.getSerial());
		entity.setTime_control(Utility.stringToTimestampUTC(dto.getTimestamp() + Utility.getSystemZoneId(), PeopleCounterDTO.TIMESTAMP_FORMAT));
    	entity.setIn_event(Integer.parseInt(dto.getIn()));
    	entity.setOut_event(Integer.parseInt(dto.getOut()));
    	
    	return entity;
	}
}
